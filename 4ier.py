#!/usr/bin/env python3

"""
Inspired by http://www.smbc-comics.com/comic/2013-02-01
"""

import sys


# Return number's representation in base as a big endian list.
def to_base(number, base):
    if base < 2:
        raise Exception("Base should be at least 2")

    if number < 0:
        raise Exception("That should be positive.")

    # If we don't handle this specially, we'll return an empty list.
    if number == 0:
        return [0]

    rep = []
    while number > 0:
        rest = number % base
        rep.append(rest)
        number = number//base

    rep.reverse()
    return rep


def count_fours(rep):
    return len([x for x in rep if x == 4])


# Returns the fouriest base for number and its representation in that base (as a big endian list).
# If there are many equally fouriest bases the smallest one will be chosen.
def fourier(number):
    base = 5 # Bases below 5 don't have 4, and therefour can't be fourier.

    fouriest_base = None
    fouriest_rep = []
    fouriest_fours = 0
    while True:
        new_rep = to_base(number, base)
        new_fours = count_fours(new_rep)

        if new_fours > fouriest_fours:
            fouriest_base = base
            fouriest_rep = new_rep
            fouriest_fours = new_fours

        # If either of these conditions is true, there is no hope of getting fourier.
        if len(new_rep) < fouriest_fours or base > number + 1:
            return (fouriest_base, fouriest_rep)

        base += 1


if __name__ == "__main__":
    if(len(sys.argv)) < 2:
        print("Missing argument")
        sys.exit(1)

    try:
        num = int(sys.argv[1])
    except:
        print("Couldn't parse number")
        sys.exit(1)

    print(fourier(num))

